# README #

### What is this repository for? ###

This repository is for those of us who are so damn scatter brained they know they can do certain things within the Linux terminal but can't quite figure out how.  
I found myself so many times saying "man, I just figured out how to get that to work... how did I do that?" and thus all of my notes can now be dumped into one place for myself as well as other fellow Linux hackers.

### How do I get set up? ###

There is no set up.  Just clone the repo that is made of text files and go from there.  Each file can be used with the cat and grep command for quicker responses.

### Contribution guidelines ###

The only thing that I ask is that you make it short and simple.  I would like to be able to clone this for folks who are just getting started using Linux and start them in the right direction.
Ease of use is priority. If there is an item that will take more than two lines to explain please put it into it's own text file.  ie. chmodLinux.txt

### Who do I talk to? ###

That would be I me.  If you have anything to add let me know and we can work something out.